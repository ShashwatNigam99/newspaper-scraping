from newspaper import Article
import pandas as pd
from tqdm import tqdm
df = pd.read_csv('filter_toi.csv')
failed = []

articles = []

for i, url in tqdm(enumerate(df['URL'])):
    toi_article = Article(url, language="en") # en for English
    #To download the article
    attempts = 0

    while attempts < 3:
        try:
            x = toi_article.download()
            #To parse the article
            toi_article.parse()
            
            #To perform natural language processing ie..nlp
            toi_article.nlp()
    
            articles.append([df.iloc[i][1], url, toi_article.title, toi_article.text, toi_article.summary, toi_article.keywords])
            attempts = 5 # out of the loop if it appended!
        except:
            if attempts == 0:
                failed.append(i)
            attempts = attempts + 1


articles_df = pd.DataFrame( articles, columns = ["date", "url", "title", "text", "summary", "keywords"] )
articles_df.to_csv('articles_dec21.csv')

print("Number of articles failed to retrieve:", len(failed))
print("Failed",failed)

cities = []
i=0
for url in articles_df["url"]:
    split_url = url.split("/") 
    if split_url[4] == "city":
        cities.append(split_url[5])
        i+=1
    else:    
        cities.append(split_url[4])

articles_df["city"] = cities
print('Articles with city mentioned:', i)


articles_df.to_csv('articles_dec21_cities.csv')

