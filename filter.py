import pandas as pd
import nltk

list_of_words = ['corona', 'covid', 'covid-19', 'coronavirus' ,'vaccine', 'vaccination', 'covishield', 'covaxin', 'oxygen', 'plasma', 'cylinder', 'remdesivir', 'ventilator', 'bed', 'cremation', 'crematorium', 'rt-pcr', 'rtpcr']
df = pd.read_csv('toi.csv')
df = df.drop_duplicates(subset=['Headline'])
# while removing this i might be dropping /city/ links look into it
filtered_df = pd.DataFrame()

for i, headline in enumerate(df['Headline']):
    # print(headline)
    headline_text_list = nltk.word_tokenize(headline)
    text = list(map(lambda x: x.lower(), headline_text_list))
    match = list(set(text).intersection(list_of_words))
    if match:
        print(headline, match)
        filtered_df = filtered_df.append(df.iloc[i])

filtered_df = filtered_df[ ["Date", "Headline", "URL"] ]

cities = []
i=0
for url in filtered_df["URL"]:
    split_url = url.split("/") 
    if split_url[4] == "city":
        cities.append(split_url[5])
        i+=1
    else:    
        cities.append(split_url[4])

filtered_df["City"] = cities
print('with city', i)
filtered_df.to_csv('filter_toi.csv')