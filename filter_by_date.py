import pandas as pd
from datetime import date, timedelta
df = pd.read_csv('articles_dec21_cities.csv')

start_date = "2021-04-11"
after_start_date = df['date'] >= start_date

df = df.loc[after_start_date]

print(df.size)

df.to_csv("post_11apr21_articles.csv")

for index, row in df.iterrows():
        print(row['date'])
        