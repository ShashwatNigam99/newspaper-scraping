from datetime import date, timedelta
import requests
from bs4 import BeautifulSoup
import pandas as pd
from tqdm import tqdm

ctr = 44228

sdate = date(2021, 2, 1)
edate = date(2021, 6, 1)
#TESTING
# edate = date(2021, 2, 2)

toi_data = []

dates = [sdate + timedelta(days=x) for x in range((edate-sdate).days)]
for date in tqdm(dates):
    # print("Going through date:", date.day, date.month, date.year)
    ctr += 1
    # create the link
    link = "https://timesofindia.indiatimes.com/2021/{:d}/{:d}/archivelist/year-2021,month-{:d},starttime-{:d}.cms".format(date.month, date.day, date.month, ctr)
    # get the raw HTML page
    r = requests.get(link)
    # bs4 goes through the HTML
    soup = BeautifulSoup(r.content, 'html5lib')

    table = soup.find('span', attrs = { 'style': 'font-family:arial ;font-size:12;color: #006699'})

    for row in table.findAll('a'):
        # print(row.text)
        toi_data.append([date,row.text, row['href']])

df = pd.DataFrame(toi_data, columns = ["Date", "Headline", "URL"])
df.to_csv('toi.csv')